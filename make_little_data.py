import os
import numpy as np
import xppll37_mc
import sys


def do(run,N):
  r = xppll37_mc.readDataset(run)
  o1 = r.opal0[:N]
  o2 = r.opal1[:N]
  np.savez("littleData/run%04d.npz" % run, opal0 = o1, opal1 = o2 ) 

if __name__ == "__main__":
  run = int(sys.argv[1])
  N=100
  do(run,N)
