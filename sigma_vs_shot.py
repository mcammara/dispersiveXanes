import matplotlib.pyplot as plt
import numpy as np
import xppl37_spectra
import xanes_analyzeRun
from datastorage import DataStorage as ds
r=xppl37_spectra.xanes_analyzeRun.AnalyzeRun(81)
r.load()
if len(r.results.keys()) > 1:
  p1 = np.vstack( ( r.results[c].p1 for c in range(0,10,2) ) )
  p2 = np.vstack( ( r.results[c].p2 for c in range(0,10,2) ) )
else:
  p1 = r.results[0].p1
  p2 = r.results[0].p2
N = int(len(p1)/2)

ref = slice(0,1000)
sam = slice(1000,1200)

#ref = slice(200)
#sam = slice(200)

ref = ds(p1=p1[ref],p2=p2[ref])
sam = ds(p1=p1[sam],p2=p2[sam])

# "ratioOfAverage medianOfRatios"
data=xppl37_spectra.calcAbs(ref,sam,refKind="medianOfRatios")

abs = data[-1]
idx = slice(200,800)
abs = abs[:,idx]

fom = []
fom1 = []
x = np.arange(abs.shape[1])
N = range(1,len(abs))
#N = 2**np.arange(15)
for i in N:
  av = np.nanmean(abs[:i],axis=0)
  p = np.polyfit(x,av,5)
  diff = av-np.polyval(p,x)
  fom.append( np.nanstd(av) )
  fom1.append( np.nanstd(diff) )

