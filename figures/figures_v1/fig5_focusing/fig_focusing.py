import sys
sys.path.insert(0,"../../../")
import collections
import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec

import dispersiveXanes_utils as utils
import xppl37_spectra
import xanes_analyzeRun
import mcutils as mc
import trx
import datastorage as ds


nice_colors = ["#1b9e77", "#d95f02", "#7570b3"]
nice_colors = "#1f78b4 #a6cee3 #b2df8a #33a02c".split()
gradual_colors = ['#014636', '#016c59', '#02818a', '#3690c0', '#67a9cf', '#a6bddb', '#d0d1e6']#, '#ece2f0']

def get_data(run=127,threshold=0.05,force=False):
  fname = "../data/fig_focusing_run%04d.h5" % run
  if not os.path.isfile(fname) or force:
    # this functions splits the run based on FOM 
    data=xppl37_spectra.calcSpectraForRun(run)
    E = data.run.E
    ref    = ds.DataStorage(E=E, p1=data.p1[0],p2=data.p2[0] )
    sample = ds.DataStorage(E=E, p1=data.p1[1],p2=data.p2[1] )

    _,_,Abs = xppl37_spectra.calcAbs(ref,ref,threshold=threshold)
    ref.Abs = Abs

    _,_,Abs = xppl37_spectra.calcAbs(ref,sample,threshold=threshold)
    sample.Abs = Abs

    temp = ds.DataStorage( ref=ref,sample=sample)
    temp.info="Abs calculated with threshold = %.3f" % threshold
    temp.save(fname)
  data = ds.read(fname)
  ref = data.ref
  sample = data.sample
  _,_,Abs = xppl37_spectra.calcAbs(ref,ref,threshold=threshold)
  ref.Abs = Abs
  _,_,Abs = xppl37_spectra.calcAbs(ref,sample,threshold=threshold)
  sample.Abs = Abs
  data = ds.DataStorage( ref=ref,sample=sample)
  data["threshold"]=threshold
  return data


def get_ref():
  E,data=np.loadtxt("../data/Fe_ref.txt",unpack=True)
  return ds.DataStorage(E=E*1e3,data=data/2.05+0.07)

def get_1b():
  E,data=np.loadtxt("../data/Fe_1bunch.txt",unpack=True)
  return ds.DataStorage(E=E*1e3,data=data/2.05+0.07)

def fig_focusing(run=127,force=False,threshold=0.05,smoothWidth=0.3,i0_monitor=0.1):
  ref = get_ref()
  color_ss = '#08519c'
  color_av = '#238b45'
  color_av_all = '#d95f0e'
  shifty = 1
  data = get_data(run,force=force,threshold=threshold)
  E = data.ref.E
  shots = range(30,35)
  figure = plt.figure(figsize = [7,5])
  gs = gridspec.GridSpec(1, 2, width_ratios=[1, 1],)
  ax = []
  ax.append( plt.subplot(gs[0]) )
  ax.append( plt.subplot(gs[1],sharex=ax[0],sharey=ax[0]) )
# ax.append( plt.subplot(gs[2],sharex=ax[0]) )

  if i0_monitor is not None:
    i0_ref = np.nanmean(data.ref.p1,axis=1)
    idx    = i0_ref>np.percentile(i0_ref,i0_monitor)
    data.ref.Abs = data.ref.Abs[idx]

    i0_ref = np.nanmean(data.sample.p1,axis=1)
    idx    = i0_ref>np.percentile(i0_ref,i0_monitor) 
    data.sample.Abs = data.sample.Abs[idx]

  ref = data.ref.Abs[shots]
  sam = data.sample.Abs[shots]
  if smoothWidth > 0:
    ref = xppl37_spectra.smoothSpectra(E,ref,res=smoothWidth) 
    sam = xppl37_spectra.smoothSpectra(E,sam,res=smoothWidth) 
  idx = E>7150
  sam[:,idx]=np.nan
  ref[:,idx]=np.nan
  av_ref = np.nanmedian(ref,0)
  av_sam = np.nanmedian(sam,0)
  for ishot,shot in enumerate(shots):
    ax[0].axhline(ishot*shifty,ls='--',color="0.9")
    ax[1].axhline(ishot*shifty,ls='--',color="0.9")
    ax[0].plot(E,av_ref+ishot*shifty,color=color_av_all,lw=1,zorder=10)
    ax[1].plot(E,av_sam+ishot*shifty,color=color_av_all,lw=1,zorder=10)
    ax[0].plot(E,ref[ishot]+ishot*shifty,ls = '-' ,color=color_ss,lw=2)
    ax[0].text(7125,ishot+0.2,"σ = %.2f"%np.nanstd(ref[ishot]))
    ax[1].plot(E,sam[ishot]+ishot*shifty,ls = '-' ,color=color_ss,lw=2)
    #to_save.append(s1_norm)
    #to_save.append(s2_norm)
    #to_save.append(a)
  ax[0].set_title("Run %s"%str(run))
  ax[0].set_ylabel("No sample Absorption")
  ax[1].set_ylabel("Sample Absorption")

  ax[0].set_xlabel("Energy (keV)")
  ax[1].set_xlabel("Energy (keV)")
  ax[0].grid(axis='x',color="0.7",lw=0.5)
  ax[1].grid(axis='x',color="0.7",lw=0.5)
  ax[0].set_xlim(7080,7150)
  ax[0].set_yticks( () )
  ax[1].set_yticks( () )
  ax[0].set_ylim(-0.3,len(ref)+0.2)
  plt.tight_layout()
  
  plt.savefig("fig_focusing.png",transparent=True,dpi=300) 
  plt.savefig("fig_focusing.pdf",transparent=True)

  to_save = np.vstack( (E,np.nanmedian(ref,0),ref) )
  info = "# threshold=%.2f; smoothWidth=%.2f eV" %(threshold,smoothWidth)
  info += "\n#E abs_average_over_shots shots ..."
  trx.utils.saveTxt("../data/fig_focusing_run%04d_ref.txt"%run,E,to_save,info=info)

  to_save = np.vstack( (E,np.nanmedian(sam,0),sam) )
  info = "# threshold=%.2f; smoothWidth=%.2f eV" %(threshold,smoothWidth)
  info += "\n#E abs_average_over_shots shots ..."
  trx.utils.saveTxt("../data/fig_focusing_run%04d_sam.txt"%run,E,to_save,info=info)


  

#if __name__ == "__main__": fig_fe_xas()
 
