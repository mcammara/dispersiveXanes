import sys
sys.path.insert(0,"../../../")

import numpy as np
import matplotlib.pyplot as plt
import xanes_analyzeRun


def run80():
  pars = dict( transx = 60, transy = -20, scalex = 0.9, scaley = 1.1, rotation = -0.03 )
  r=xanes_analyzeRun.AnalyzeRun(80,initAlign=pars)
  # NOTE: calib0 has sample in, so alignment has to be done with odd calibcycles
  r.doShots(shots=slice(3,10),calib=1,doFit=True)
  r.analyzeScan()
  r.save(overwrite=True)

def run82():
  pars =  dict( rotation =  -0.03, scaley = 0.85, transx = 48, transy = 25.8 )
  r=xanes_analyzeRun.AnalyzeRun(82,initAlign=pars)
  r.doShots(shots=slice(5,10),calib=0,doFit=True)
  r.analyzeScan()
  r.save(overwrite=True)


def run84():
  # run 84 is IN/OUT scan and theta scan
  r=xanes_analyzeRun.AnalyzeRun(84)
  r.analyzeScan(calibsToFit='even', nImagesToFit=5)
  r.save(overwrite=True)
